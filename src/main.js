//Aliases
let Application = PIXI.Application,
    Container = PIXI.Container,
    loader = PIXI.loader,
    resources = PIXI.loader.resources,
    TextureCache = PIXI.utils.TextureCache,
    Sprite = PIXI.Sprite,
    Rectangle = PIXI.Rectangle,
    Text = PIXI.Text,
    TextStyle = PIXI.TextStyle;


function preinit () {
    let type = "WebGL"
    if(!PIXI.utils.isWebGLSupported()){
        type = "canvas"
    }
    init();
}

let app;

function init() {

    //Create a Pixi Application
    app = new PIXI.Application({ 
        width: 1286,                // default: 800
        height: 640,                // default: 600
        antialias: false,            // default: false
        transparent: false,         // default: false
        resolution: 1               // default: 1
    });

    //Add the canvas that Pixi automatically created for you to the HTML document
    document.body.appendChild(app.view);

    pixiloader();
}

function pixiloader() {
    PIXI.loader
    .add([
        "assets/column.png",
        "assets/playButton.png"
    ])
    .add("assets/WorldAssets.json")
    .on("progress", loadProgressHandler)
    .load(setup);

}

function loadProgressHandler(loader, resource) {
    //Display the file `url` currently being loaded
    console.log("loading: " + resource.url); 

    //Display the percentage of files currently loaded
    console.log("progress: " + loader.progress + "%"); 

    //If you gave your files names as the first argument 
    //of the `add` method, you can access them like this
    //console.log("loading: " + resource.name);
}
// PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST


/////////////////////
// Globals (setup) //
/////////////////////
let sprite, state, gravity, obstacle, message;
let bg_05far;
function setup() {
    console.log("All files loaded!"); 

    let bg_sprites = [];

    //Create the obstacle
    // obstacle = new PIXI.Graphics();
    // obstacle.beginFill(0xCCFF99);
    // obstacle.drawRect(0, 0, 64, 64);
    // obstacle.endFill();
    // obstacle.x = 120;
    // obstacle.y = 96;
    // app.stage.addChild(obstacle);

    //placeholder sprite created here
    let id = PIXI.loader.resources["assets/WorldAssets.json"].textures;

    // let bg_05far;
    // bg_05far = new Sprite (id["05_far_BG.jpg"]);
    // bg_05far.x = 0;
    // bg_05far.y = 0;
    // // bg_sprites.push(bg_05far);
    // // console.log(bg_sprites[0]._texture.textureCacheIds[0]);
    // app.stage.addChild(bg_05far);    

    let bg_05far_texture;
    bg_05far_texture = id["05_far_BG.jpg"];
    bg_05far_texture.scale = 1; //why won't it scale???
    
    // bg_05far = new PIXI.extras.TilingSprite(bg_05far_texture, app.screen.width, bg_05far_texture.height);
    bg_05far = new PIXI.extras.TilingSprite(bg_05far_texture, bg_05far_texture.baseTexture.width, bg_05far_texture.height);
    bg_05far.x = 0;
    bg_05far.y = 0;
    app.stage.addChild(bg_05far);

    // console.log(bg_05far_texture);

    // let bg_03rearSilhouette;
    // bg_03rearSilhouette = new Sprite (id["03_rear_silhouette.png"]);
    // bg_03rearSilhouette.x = 0;
    // bg_03rearSilhouette.y = 0;
    // bg_sprites.push(bg_03rearSilhouette);
    // app.stage.addChild(bg_03rearSilhouette);
    // let bg_03rearCanopy;
    // bg_03rearCanopy = new Sprite (id["03_rear_canopy.png"]);
    // bg_03rearCanopy.x = 0;
    // bg_03rearCanopy.y = 0;
    // bg_sprites.push(bg_03rearCanopy);
    // app.stage.addChild(bg_03rearCanopy);    

    // let bg_02Tree1;
    // bg_02Tree1 = new Sprite (id["02_tree_1.png"]);
    // bg_02Tree1.x = 0;
    // bg_02Tree1.y = 0;
    // bg_sprites.push(bg_02Tree1);
    // app.stage.addChild(bg_02Tree1);

    // let bg_02Tree2;
    // bg_02Tree2 = new Sprite (id["02_tree_2.png"]);
    // bg_02Tree2.x = 0;
    // bg_02Tree2.y = 0;
    // bg_sprites.push(bg_02Tree2);
    // app.stage.addChild(bg_02Tree2);

    // sprite = new Sprite (id["pickup_02.png"]);
    // sprite.x = 120;
    // sprite.y = 500;
    // sprite.vx = 0;
    // sprite.vy = 0;

    // gravity = 9.81;

    // app.stage.addChild(sprite);

    // setupInput();

    // example text
    // textStyle();

    // //Create the text sprite
    // let style = new TextStyle({
    //     fontFamily: "sans-serif",
    //     fontSize: 18,
    //     fill: "white",
    // }); 
    // message = new Text("No collision...", style);
    // message.position.set(8, 8);
    // app.stage.addChild(message);

    //Set the game state
    state = play;

    //Start the game loop by adding the `gameLoop` function to
    //Pixi's `ticker` and providing it with a `delta` argument.
    app.ticker.add(delta => gameLoop(delta));    
}

// let isDown = false;

function mouseDownHandler () {
    console.log('clicked/tapped');
    // console.log(isDown);
    // if(!isDown) {
        sprite.vy = -20;
    //     isDown = true;
    //     console.log(isDown);
    // }
}
function mouseUpHandler () {
    // sprite.vy = 0;
    // isDown = false;
    // console.log('mouseup ' + sprite.vy);
}

function setupInput() {
    //Pointers normalize touch and mouse
    // app.stage.on('pointerdown', onClick);
    document.addEventListener('mouseup',mouseUpHandler.bind(this));
    document.addEventListener('mousedown',mouseDownHandler.bind(this));
}

function gameLoop(delta){
    //Update the current game state:
    state(delta);
}

function play(delta) {

    // //check for a collision between the cat and the obstacle
    // if (hitTestRectangle(sprite, obstacle)) {

    //     //if there's a collision, change the message text
    //     //and tint the obstacle red
    //     message.text = "hit!";
    //     obstacle.tint = 0xff3300;
    // } else {

    //     //if there's no collision, reset the message
    //     //text and the obstacle's color
    //     message.text = "No collision...";
    //     obstacle.tint = 0xccff99;
    // }

    // //Use the sprite's velocity to make it move
    // sprite.x += sprite.vx;
    // sprite.y += sprite.vy;
    
    // //Flight physics
    // if(sprite.vy < 0) {
    //     sprite.vy++;
    // }
    // sprite.y += gravity;
    // // console.log(sprite.vy);
    bg_05far.tilePosition.x -= 1;
}

function pause(delta) {
    console.log('paused');
}

function textStyle() {
    let style = new TextStyle({
        fontFamily: "Fredoka One",
        fontSize: 36,
        fill: "white",
        stroke: '#ff3300',
        strokeThickness: 4,
        dropShadow: true,
        dropShadowColor: "#000000",
        dropShadowBlur: 4,
        dropShadowAngle: Math.PI / 6,
        dropShadowDistance: 6,
      });
    let message = new Text("Hello Pixi!", style);
    app.stage.addChild(message);
    message.position.set(54, 96);
}

// nice little random number function
function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

//Hit detection function
function hitTestRectangle(r1, r2) {

    //Define the variables we'll need to calculate
    let hit, combinedHalfWidths, combinedHalfHeights, vx, vy;
  
    //hit will determine whether there's a collision
    hit = false;
  
    //Find the center points of each sprite
    r1.centerX = r1.x + r1.width / 2;
    r1.centerY = r1.y + r1.height / 2;
    r2.centerX = r2.x + r2.width / 2;
    r2.centerY = r2.y + r2.height / 2;
  
    //Find the half-widths and half-heights of each sprite
    r1.halfWidth = r1.width / 2;
    r1.halfHeight = r1.height / 2;
    r2.halfWidth = r2.width / 2;
    r2.halfHeight = r2.height / 2;
  
    //Calculate the distance vector between the sprites
    vx = r1.centerX - r2.centerX;
    vy = r1.centerY - r2.centerY;
  
    //Figure out the combined half-widths and half-heights
    combinedHalfWidths = r1.halfWidth + r2.halfWidth;
    combinedHalfHeights = r1.halfHeight + r2.halfHeight;
  
    //Check for a collision on the x axis
    if (Math.abs(vx) < combinedHalfWidths) {
  
      //A collision might be occuring. Check for a collision on the y axis
      if (Math.abs(vy) < combinedHalfHeights) {
  
        //There's definitely a collision happening
        hit = true;
      } else {
  
        //There's no collision on the y axis
        hit = false;
      }
    } else {
  
      //There's no collision on the x axis
      hit = false;
    }
  
    //`hit` will be either `true` or `false`
    return hit;
};


window.onload = preinit();