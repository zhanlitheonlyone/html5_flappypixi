var app = {

    // Global variables to do with web/mobile settings

    //app.pixi, app.id
    //app.gameScene, app.gameOverScene
    //app.state
    scoreCur:0, //app.curScore
    isScored:false,
    anim: {
        phase1:true,
        phase2:false
    },
 
	init : function(){

        app.initPixi();
        // app.setScale();

    },

    initPixi : function(){
        
        app.pixi = new PIXI.Application({ 
            width: 960,                 // default: 800
            height: 640,                // default: 600
            antialias: true,            // default: false
            transparent: false,         // default: false
            resolution: 1               // default: 1
        });
        // app.pixi.renderer.autoResize = true;
        // app.pixi.renderer.resize(800, 600);
        document.getElementById("gameHolder").appendChild(app.pixi.view);

        app.initPreload();
    },

    mouseDownHandler : function () {
        // console.log('clicked/tapped');
        switch(app.state) {
            case app.play:
                //when game is playing
                // console.log("GAME");
                app.player.vy = -20;
                break;
            case app.gameOver:
                //when the game ends
                console.log("GAMEOVER");
                app.resetGame();
                break;
            case app.onGameStart:
                //when the game is waiting to start
                // app.onGameStart();
                // console.log("START");
                break;
            case app.gameWait:
                //when the game is waiting to start
                app.onGameStart();
                // console.log("WAIT");
                break;                
            default:
                console.log("NOTHING");
        }
        document.getElementById("webBtn").style.backgroundImage = "url('assets/flapBtn_down.png')";
    },
    mouseUpHandler : function () {
        console.log('mouseUP');
        var now = (new Date()).getTime();
        if (now - app.lastTouchEnd <= 1000) {
            event.preventDefault();
        }
        app.lastTouchEnd = now;
        document.getElementById("webBtn").style.backgroundImage = "url('assets/flapBtn_up.png')";
    },

    play : function(delta) {

        app.onGamePlay();

        app.player.y += app.player.vy;

        // Check collision with the ground/lava
        if (app.player.y > app.pixi.screen.height - app.player.height) {
            app.state = app.gameOver;
            app.onGameOver();
        }

        if(app.player.vy < 0) {
            app.player.vy++;
        }
        app.player.y += app.gravity;
        // console.log(app.player.vy);

        // console.log("playing");
        for (let i = 0; i < app.obstacles.length; i++) {

            // app.obstacles[i].x = 200 * i; //test

            if(app.obstacles[i].isMoving) {
                app.obstacles[i].x -= app.obstacles[i].vx;
            }

            // check when the next obstacle can start to move
            // here it triggers at the halfway point of the PIXI canvas screen.
            // can later script a 'rate of fire' for the columns to put inside the conditional
            if (app.obstacles[i].x < app.gameScene.width/2 - app.obstacles[i].width) {
                
                // start the next obstacle in the array
                if(i < app.obstacles.length-1) {
                    // console.log("setting next one to move");
                    app.obstacles[i+1].isMoving = true;

                } else {
                    app.obstacles[0].isMoving = true;
                }
                
            }
       
            if (app.obstacles[i].x < 0-app.obstacles[i].width) {
                // console.log("obstacle "+[i]+" is past 0");
                // app.obstacles[i].x = app.gameScene.width;
                // reset the position of the obstacle
                app.obstacles[i].resetPos();
            }
            
            // make an object here for collision function
            let obstacleData1 = {}
            obstacleData1.x = app.obstacles[i].children[0].getGlobalPosition().x;
            obstacleData1.y = app.obstacles[i].children[0].getGlobalPosition().y;
            obstacleData1.width = app.obstacles[i].children[0].width;
            obstacleData1.height = app.obstacles[i].children[0].height;
            let obstacleData2 = {}
            obstacleData2.x = app.obstacles[i].children[1].getGlobalPosition().x;
            obstacleData2.y = app.obstacles[i].children[1].getGlobalPosition().y;
            obstacleData2.width = app.obstacles[i].children[1].width;
            obstacleData2.height = app.obstacles[i].children[1].height;

            // Check collision with columns
            if(app.hitTestRectangle(app.player, obstacleData1)) {
                // app.pixiHit = true;
                // maybe put these kinds of globals into a .settings object...?
                console.log("HIT");
                app.state = app.gameOver;
                app.onGameOver();
            }
            // Check collision with columns
            if(app.hitTestRectangle(app.player, obstacleData2)) {
                console.log("HIT");
                app.state = app.gameOver;
                app.onGameOver();
            }

            if(app.hitTestRectangle(app.player, app.obstacles[i]) && !app.isScored) {
                
                console.log("HIT FOR SCORE");
                app.updateScore();
                
            }   // not sure of logic pattern to make sure that score only increments by 1, rather than with each time 
                // the play loop runs. I think it's a coroutine, but I have not yet learned how to do this in JS.
                // instead, I'm going to filthily use a setTimeout in app.updateScore(); I know I shouldn't but 
                // I'm conscious of time...
            // else if (app.hitTestRectangle(app.player, app.obstacles[i]) === false && app.isScored) {
            //     console.log("NOT HITTING");
            //     app.isScored = false;
            // }
        }// /for

        // app.obstacles.forEach(function(obstacle){
        //     if(obstacle.isMoving) {
        //         obstacle.x -= obstacle.vx;
        //     }
        //     // check when the next obstacle can start to move
        //     // here it triggers at the halfway point of the PIXI canvas screen.
        //     // can later script a 'rate of fire' for the columns to put inside the conditional
        //     if (obstacle.x < app.gameScene.width/2 - obstacle.width) {
                
        //         // start the next obstacle in the array
        //         if(i < app.obstacles.length-1) {
        //             // console.log("setting next one to move");
        //             app.obstacles[i+1].isMoving = true;

        //         } else {
        //             app.obstacles[0].isMoving = true;
        //         }
                
        //     }       
        // });

        /////////////////////////
        // Background Movement //
        /////////////////////////
        app.bg_05.tilePosition.x -= 1;
        app.bg_03_rearSil.tilePosition.x -= 3;
        app.bg_03_rearCan.tilePosition.x -= 3;
        app.bg_02_frontCan.tilePosition.x -= 5;
        app.bg_01_frontSil.tilePosition.x -= 5;

    },

    updateScore : function() {

        // update the score here - text stuff
        app.scoreCur++;
        app.scoreText.text = app.scoreCur;
        app.isScored = true;
        setTimeout(function(){app.isScored = false},600); // I feel so dirty >__<;; I'm sorry!
        // add highscore feature in future here?

    },

	gameLoop : function(delta){

        //Update the current game state:
        // state(delta);
        
        // setTimeout(function(){app.dispatcher.dispatch( app.EVENTS.GAME_OVER );}, 3000)

        app.state(delta);
        // console.log("TICK");

    },

    initPreload : function() {

        //pixi.js loader, load all game assets here into textures cache
        // 0. load all assets into texture cache
        //    - assets/column.png
        //    - assets/flyingPixie.png
        //    - assets/playButton.png
        //    - assets/WorldAssets.json
       
        app.pixi.loader
        .add([
            "assets/column.png",
            "assets/playButton.png",
            "assets/flyingPixie.png"
        ])
        .add("assets/WorldAssets.json")
        .on("progress", app.loadProgressHandler)
        .load(app.setupEntities);

    },

    loadProgressHandler : function (loader, resource) {

        //Display the file `url` currently being loaded
        console.log("loading: " + resource.url); 
    
        //Display the percentage of files currently loaded
        console.log("progress: " + loader.progress + "%"); 
    
        //If you gave your files names as the first argument 
        //of the `add` method, you can access them like this
        //console.log("loading: " + resource.name);

    },

	setupEntities : function(){

        console.log("[RUN ORDER] setupEntities run");
        console.log("stage width at setupEntities() is "+app.pixi.stage.width);

        //Create the `gameScene` group
        app.gameScene = new PIXI.Container();
        // setting the container height to be same as screen height seems iffy... reconsider...
        app.gameScene.width = app.pixi.screen.width;
        app.gameScene.height = app.pixi.screen.height;
        app.pixi.stage.addChild(app.gameScene);
        app.gameScene.visible = false;
        // app.gameScene.width = 800;

        //Create a `gameOverScene` group
        app.gameOverScene = new PIXI.Container();
        app.pixi.stage.addChild(app.gameOverScene);
        app.gameOverScene.visible = false;
        
        app.id = app.pixi.loader.resources["assets/WorldAssets.json"].textures;
        
        app.setBackground();

        app.setPlayer();

        // app.setPickups();
        // app.setColumn();
        // app.initPreloadScreen();

        // app.setControl();

        // app.setAudio();
        
        //set app.obstacle off-screen
        // app.obstacle.x = app.gameScene.width;

        let obstaclesCount = 5;
        app.obstacles = [];

        //Make as many columns as there are `obstaclesCount`
        for (let i = 0; i < obstaclesCount; i++) {

            let tempObstacle = new PIXI.Container();
            
            //Make a column
            // app.setColumn(tempObstacle, i);

            // how big the gap between columns will be.
            tempObstacle.gapSize = 200;

            // saving sprite to main app scope
            app.columnTop = new PIXI.Sprite(app.pixi.loader.resources["assets/column.png"].texture);
            app.columnBot = new PIXI.Sprite(app.pixi.loader.resources["assets/column.png"].texture);
            
            tempObstacle.totalColumnHeight = 
                app.columnTop.y + app.columnTop.height +
                app.columnBot.y + app.columnBot.height + tempObstacle.gapSize;
                // console.log(tempObstacle.totalColumnHeight);

            tempObstacle.gapPosY = app.pixi.screen.height/2; //use with the screen height? or stage height?

            // app.columnTop.y = tempObstacle.gapPosY - (tempObstacle.totalColumnHeight/2);
            // app.columnBot.y = app.columnTop.y + app.columnTop.height + tempObstacle.gapSize;

            tempObstacle.addChild(app.columnTop);
            tempObstacle.addChild(app.columnBot);

            // set the position of the gap here - use randomInt function
            tempObstacle.setGapPos = function() {
                this.gapPosY = app.randomInt(200, app.pixi.screen.height-200);
                // console.log(this.gapPosY);
                // this.children[0];
                this.children[0].y = this.gapPosY - (this.totalColumnHeight/2);
                this.children[1].y = this.children[0].y + this.children[0].height + this.gapSize;
                // Set app.columnTop.y to determine gap vertical position
            }

            tempObstacle.setGapPos();
            
            // let x = spacing * i + xOffset; 
            // set x to be off screen on the right
            let x = app.pixi.screen.width + tempObstacle.width;
            //set velocity of obstacle
            let vx = 7;
            //Give the blob a random y position
            // let y = randomInt(0, app.stage.height - blob.height);

            //Set initial column position
            tempObstacle.x = x;
            //Set initial column velocity
            tempObstacle.vx = vx;
            //Set obstacle's isMoving bool
            tempObstacle.isMoving = false;

            tempObstacle.resetPos = function() {
                // console.log("obstacle position reset with new y pos!");
                this.x = x;
                this.setGapPos();
                this.isMoving = false;
            }

            //Push current obstacle to the array
            app.obstacles.push(tempObstacle);

            //Add the obstacle to the scene
            app.gameScene.addChild(tempObstacle);
        }       

        //set first obstacle to be allowed to move
        app.obstacles[0].isMoving = true;

        app.gravity = 9.81; //figure associated with Earth's gravity scale.

        // set input controls
        app.setupInput();

        // There is a pixel ratio discrepancy somewhere?!?!?!?!?
        //                                  v                      v
        app.bg_05.y = (app.gameScene.height/4) - (app.bg_05.height/2);

        app.state = app.gameWait; //waiting for game to start
        // is this the right place to initialise tick?
        app.pixi.ticker.add(delta => app.gameLoop(delta)); 
        // app.pixi.stage.width = 800;
        // app.pixi.stage.height = 600;

        // app.pixi.stage.width is only set by the contents of its children.
        // I don't know how to control the width without skewing the sprites
        // that are loaded into the stage already.

        //Create the `gameOver` scene
        app.gameOverScene = new PIXI.Container();
        app.pixi.stage.addChild(app.gameOverScene);
        //Make the `gameOver` scene invisible when the game first starts
        app.gameOverScene.visible = false;

        //Create the `gameStart` scene
        app.gameStartScene = new PIXI.Container();
        app.pixi.stage.addChild(app.gameStartScene);
        app.gameStartScene.visible = true;

        //to have higher in the layer stack
        app.setPlayBtn();
        app.setText();



	},      

    /////////////////////
    // Create entities //
    /////////////////////

    setPlayer : function() {

        app.player = new PIXI.Sprite(app.pixi.loader.resources["assets/flyingPixie.png"].texture);
        app.player.x = 400;
        app.player.y = app.pixi.screen.height/2;
        // app.player.anchor.set(0.5,0.5);
        // app.player.anchor.x = 0.5; 
        // I've coded the game with anchor 0,0, can't center anchor it without complications
        app.player.vx = 0;
        app.player.vy = 0;
        app.gameScene.addChild(app.player);
        
    },

    setColumn : function(theContainer, iteration) {

        // column needs to be a prefab
        // each column object needs:
        // 2 column sprites, with a gap
        // a y position
        // an x position
        // a function to move it from right to left
        // a function to reset the position to off-screen right when it goes off-screen left

        // column sprite is reporting as 139, 540 for some reason,
        // even though column.png is actually 185, 720. 
        // I don't understand this rendering issue.

        // how big the gap between columns will be.
        let gapSize = 200;

        // create container for the obstacle (i.e. 2 column.png)
        // app.obstacle = new PIXI.Container();
        // app.obstacle.x = app.gameScene.width;
        // app.gameScene.addChild(app.obstacle);
        
        app.columnTop = new PIXI.Sprite(app.pixi.loader.resources["assets/column.png"].texture);
        app.columnBot = new PIXI.Sprite(app.pixi.loader.resources["assets/column.png"].texture);
        
        let totalColumnHeight = 
            app.columnTop.y + app.columnTop.height +
            app.columnBot.y + app.columnBot.height + gapSize;
            // console.log(totalColumnHeight);
        let gapPosY = app.pixi.screen.height/2; //use with the screen height? or stage height?

        // Set app.columnTop.y to determine gap vertical position
        app.columnTop.y = gapPosY - (totalColumnHeight/2);
        app.columnBot.y = app.columnTop.y + app.columnTop.height + gapSize;
        
        app.obstacle.addChild(app.columnTop);
        app.obstacle.addChild(app.columnBot);
        // app.obstacle.x = 300;
        // app.obstacle.y = 0;
        // app.obstacle.vx = 0;
        // app.obstacle.vy = 0;
        // app.obstacle.texture.baseTexture.mipmap = true;
        // app.gameScene.addChild(app.obstacle);
        // console.log(app.obstacle.width+" "+app.obstacle.height);
    },

    setBackground : function() {

        //Deal with scrolling background
        //TilingSprite
        // app.bg_05 = new PIXI.Sprite(app.id["05_far_BG.jpg"]); //test
        let bg_05_texture;
        bg_05_texture = app.id["05_far_BG.jpg"];
        app.bg_05 = new PIXI.extras.TilingSprite(bg_05_texture, bg_05_texture.baseTexture.width, bg_05_texture.height);
        // app.bg_05.scale.x = 2;
        // app.bg_05.scale.y = 2;
        // app.bg_05.scale.set(2,2);
        // console.log(app.pixi.stage.height/2); // why does this report that it's 0?
        // app.bg_05.y = app.pixi.stage.height/2;
        // app.bg_05.anchor.y = 0.5;

        let bg_03_rearSil_texture;
        bg_03_rearSil_texture = app.id["03_rear_silhouette.png"];
        app.bg_03_rearSil = new PIXI.extras.TilingSprite(bg_03_rearSil_texture, bg_03_rearSil_texture.baseTexture.width, bg_03_rearSil_texture.height);
        app.bg_03_rearSil.y = app.pixi.screen.height - app.bg_03_rearSil.height - 40;

        let bg_03_rearCan_texture;
        bg_03_rearCan_texture = app.id["03_rear_canopy.png"];
        app.bg_03_rearCan = new PIXI.extras.TilingSprite(bg_03_rearCan_texture, bg_03_rearCan_texture.baseTexture.width, bg_03_rearCan_texture.height);
        app.bg_03_rearCan.y = 40;

        let bg_02_frontCan_texture;
        bg_02_frontCan_texture = app.id["02_front_canopy.png"];
        app.bg_02_frontCan = new PIXI.extras.TilingSprite(bg_02_frontCan_texture, bg_02_frontCan_texture.baseTexture.width, bg_02_frontCan_texture.height);
        app.bg_02_frontCan.y = 0;

        let bg_01_frontSil_texture;
        bg_01_frontSil_texture = app.id["01_front_silhouette.png"];
        app.bg_01_frontSil = new PIXI.extras.TilingSprite(bg_01_frontSil_texture, bg_01_frontSil_texture.baseTexture.width, bg_01_frontSil_texture.height);
        app.bg_01_frontSil.y = app.pixi.screen.height - app.bg_01_frontSil.height;

        app.gameScene.addChild(app.bg_05);
        app.gameScene.addChild(app.bg_03_rearCan);
        app.gameScene.addChild(app.bg_03_rearSil);
        app.gameScene.addChild(app.bg_02_frontCan);
        app.gameScene.addChild(app.bg_01_frontSil);

        // create tree function that would create array to randomise trees that would scroll by

        // create hanging flower function that would create array to randomise hanging flower groups to scroll by

    },

    setPlayBtn : function() {

        app.playBtn = new PIXI.Sprite(app.pixi.loader.resources["assets/playButton.png"].texture);
        app.playBtn.x = app.pixi.screen.width / 2;
        app.playBtn.y = app.pixi.screen.height / 2;
        app.playBtn.anchor.x = 0.5;
        app.playBtn.anchor.y = 0.5;
        // why is the stage width not the stage width I set when initiating PIXI?
        // console.log(app.pixi.stage.width+" "+app.playBtn.x);
        app.gameStartScene.addChild(app.playBtn);

        app.playBtnSml = new PIXI.Sprite(app.pixi.loader.resources["assets/playButton.png"].texture);
        app.playBtnSml.x = app.pixi.screen.width / 2;
        app.playBtnSml.y = app.pixi.screen.height / 2 + (app.playBtnSml.height/2 -60);
        app.playBtnSml.scale.set(0.5, 0.5);
        app.playBtnSml.anchor.set(0.5, 0.5);
        app.gameOverScene.addChild(app.playBtnSml);
    },

    setText : function() {
        let style = new PIXI.TextStyle({
            fontFamily: "Fredoka One",
            fontSize: 36,
            fill: "#ffffff",
            // stroke: '#ff3300',
            // strokeThickness: 4,
            // dropShadow: true,
            // dropShadowColor: "#ffffff",
            // dropShadowBlur: 4,
            // dropShadowAngle: Math.PI / 6,
            // dropShadowDistance: 6,
        });
        let message = new PIXI.Text("Score: ", style);
        app.gameScene.addChild(message);
        message.position.set(20, 10);
        app.scoreText = new PIXI.Text(app.scoreCur, style); //make score number global
        // let score = new PIXI.Text("10", style); //test
        app.gameScene.addChild(app.scoreText);
        app.scoreText.position.set(135, 10);

        app.gameoverText = new PIXI.Text("GAME OVER", style);
        app.gameOverScene.addChild(app.gameoverText);
        app.gameoverText.anchor.x = 0.5;
        app.gameoverText.anchor.y = 0.5;
        app.gameoverText.position.set(app.pixi.screen.width/2, app.pixi.screen.height/2-60);
        
    },

    setupInput : function () {
        //Pointers normalize touch and mouse
        // app.stage.on('pointerdown', onClick);
        // document.addEventListener('mouseup',mouseUpHandler.bind(this));
        document.addEventListener('mousedown',app.mouseDownHandler);
        document.addEventListener("touchstart", app.mouseDownHandler);
        document.addEventListener('mouseup',app.mouseUpHandler);
        document.addEventListener("touchend", app.mouseUpHandler);
        document.addEventListener('touchmove', function (event) {
            if (event.scale !== 1) { event.preventDefault(); }
          });
        app.lastTouchEnd = 0;
    }, 

    removeInput : function () {
        document.removeEventListener('mousedown',app.mouseDownHandler);
        document.removeEventListener("touchstart", app.mouseDownHandler);
    },

    //////////////////
    // Game Systems //
    //////////////////

    flapPhysics : function() {
        // //Flight physics
        // //Reduce the velocity of the flap velocity.
        // if(sprite.vy < 0) {
        //     sprite.vy++;
        // }
        // sprite.y += gravity;
    },

    runColumns : function() {

        //Control columns
        //Set two, with a gap
        //Set random y pos of the gap
        //Set animation that moves columns
        //Set means of looping columns and randomizing them again
        //Collision detection of columns and player?

    },

    ///////////////////////
    // Game State Scenes //
    ///////////////////////

    gameOver : function() {
        // death animation here, with game over text
        // setTimeout(function(){app.onGameOver();},2000);
        
        if(app.player.y > 100 && app.anim.phase1) {
            app.player.y -= 20;
        } else {
            app.anim.phase1 = false;
        }
        if(!app.anim.phase1) {
            app.player.y += 30;
        }
        app.player.rotation -= .25;
    },

    onGameOver : function() {

        console.log("gameover!");
        app.removeInput();
        setTimeout(function(){
            app.setupInput();
            app.gameScene.visible = false;
            app.gameOverScene.visible = true;
            app.gameStartScene.visible = false; 
            app.anim.phase1 = true;
        }, 1000);

    },

    onGamePlay : function() {
        // app.playBtn.visible = false;
    },

    onGameStart : function() {
        //Set the game state
        //app.state = app.play;
        // console.log("waiting for user to start");
        // setTimeout(function() {app.state = app.play;}, 3000); // test
        // app.playBtn.visible = true;

        app.gameScene.visible = true;
        app.gameOverScene.visible = false;
        app.gameStartScene.visible = false;  

        //sets state back to play loop
        app.state = app.play;
    },

    gameWait : function() {
        app.gameScene.visible = false;
        app.gameOverScene.visible = false;
        app.gameStartScene.visible = true;
    },

    resetGame : function() {
        console.log("RESET");
        //reset player position
        app.player.y = app.pixi.screen.height/2;
        app.player.vx = 0;
        app.player.vy = 0;
        app.player.rotation = 0;
        //reset columns
        for (let i = 0; i < app.obstacles.length; i++) {
            app.obstacles[i].resetPos();
        }
        //set first obstacle to be allowed to move
        app.obstacles[0].isMoving = true;
        
        //reset score
        app.scoreCur = 0;
        app.scoreText.text = app.scoreCur;
        app.isScored = false;
        
        // app.onGameStart();
        app.state = app.gameWait;
    },

    //////////////////////
    // Helper Functions //
    //////////////////////

    //Hit detection function
    hitTestRectangle : function(r1, r2) {

        //Define the variables we'll need to calculate
        let hit, combinedHalfWidths, combinedHalfHeights, vx, vy;
    
        //hit will determine whether there's a collision
        hit = false;
    
        //Find the center points of each sprite
        r1.centerX = r1.x + r1.width / 2;
        r1.centerY = r1.y + r1.height / 2;
        r2.centerX = r2.x + r2.width / 2;
        r2.centerY = r2.y + r2.height / 2;
    
        //Find the half-widths and half-heights of each sprite
        r1.halfWidth = r1.width / 2;
        r1.halfHeight = r1.height / 2;
        r2.halfWidth = r2.width / 2;
        r2.halfHeight = r2.height / 2;
    
        //Calculate the distance vector between the sprites
        vx = r1.centerX - r2.centerX;
        vy = r1.centerY - r2.centerY;
    
        //Figure out the combined half-widths and half-heights
        combinedHalfWidths = r1.halfWidth + r2.halfWidth;
        combinedHalfHeights = r1.halfHeight + r2.halfHeight;
    
        //Check for a collision on the x axis
        if (Math.abs(vx) < combinedHalfWidths) {
    
        //A collision might be occuring. Check for a collision on the y axis
        if (Math.abs(vy) < combinedHalfHeights) {
    
            //There's definitely a collision happening
            hit = true;

        } else {
    
            //There's no collision on the y axis
            hit = false;

        }
        } else {
    
            //There's no collision on the x axis
            hit = false;
        }
    
        //`hit` will be either `true` or `false`
        return hit;
    },
    
    // nice little random number function
    randomInt : function(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

}
//Start it all
window.onload = app.init;